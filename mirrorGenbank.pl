#!/usr/bin/perl -w

=head1 SYNOPSIS

mirrorGenbank.pl - mirrors the Genbank repository of complete prokaryotic chromosomes, in a lighter and more user-friendly way than NCBI does.

It downloads ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/assembly_summary_genbank.txt, filters in "Complete Genome".

It organizes the data with user-friendly folders, using for folders the name in the DEFINITION line of the genbank, replacing andy non-alnum character (a-z, A-Z, 0-9, '_') by '_' and for file names the LOCUS number. It tries to give a meaningful name, but some genomes will end up with the same name.

The original data files are decompressed, split, and saved by their locus name. The original files are kept under a _src folder

=head1 USAGE

mirrorGenbank.pl [-o|--outfolder folder] [-f|--force] [-d|--debug integer] [-n|--do-not-download-stat-file] [-s|--skip integer] [-u|--do-not-update] [-l|--local-only]

=head1 INPUT

=head2 [-o|--outfolder folder]

Folder where to put files. By default: /nobackup/genbank_mirror/

=head2 [-f|--force]

Forces re-download of files even if the file on NCBI is older than the local copy.

=head2 [-d|--debug integer]

Debug mode: outputs more info, and run only the number of files set.

=head2 [-n|--do-not-download-stat-file]

Do not download stat file again, use the one in the local folder instead.

=head2 [-s|--skip integer]

Skip a certain number of assemblies. Useful for example when first synchronization crashed.

=head2 [-l|--local-only]

Perform only local operations, i.e. organizing folders, prepping files. Essentially a cleanup operation. Forces -n|--do-not-download-stat-file.

=head1 OUTPUT

See synopsis. A tab-file is output on standard output.

=head1 AUTHOR

Lionel Guy (lionel.guy@imbim.uu.se)

=head1 DATE

Tue May 12 17:08:50 CEST 2015

=head1 COPYRIGHT

Copyright (c) 2015 Lionel Guy

=head1 LICENSE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

# libraries
use strict;
use Pod::Usage;
use Getopt::Long;
#use Net::FTP;
use File::Basename;
use File::stat;
use File::Copy;
use File::Fetch;
use Digest::MD5::File qw(file_md5 file_md5_hex);
use File::Rsync;
use PerlIO::gzip;

# variables: could go into options at some point...
my $ftpserver = 'ftp.ncbi.nlm.nih.gov';
my $rsyncserver = 'ftp.ncbi.nlm.nih.gov';
#my $rsyncserver = 'rsync.ncbi.nlm.nih.gov';
my $genomesfolder = 'genomes/all';
my $statfile = '/genomes/genbank/assembly_summary_genbank.txt';
my @filetypes = 
  qw/ genomic.fna genomic.gbff genomic.gff protein.faa /; 
my @outfields = qw/ bioproject biosample refseq_category taxid specied_taxid organism_name intraspecific_name asm_name ftp_path /;

# stats
my $nrows = 0;
my $ncomplete = 0;
my $nfiles = 0;
my $nupdatedfiles = 0;
my $nsplitfolders = 0;
#my $nuniqueName = 0;

# options
my $force;
my $debug;
my $folder = '/nobackup/genbank_mirror';
my $uselocalstat;
my $skip;
my $localonly;
my $help;
GetOptions(
	   'o|outfolder=s'               => \$folder,
	   'f|force'                     => \$force,
	   'd|debug=i'                   => \$debug,
	   'h|help'                      => \$help,
	   'n|do-not-download-stat-file' => \$uselocalstat,
	   's|skip=i'                    => \$skip,
	   'l|local-only'                => \$localonly,
	  );

pod2usage(-exitval => 1, -verbose => 2) if $help;

# Check if folder exists
if (-e $folder){
  die "File $folder not a writable directory\n" 
    unless (-d $folder && -w $folder);
} else {
  mkdir($folder) or die "Cannot create folder $folder: $!\n";
}

# -l forces -n
$uselocalstat++ if $localonly;

####################################
# Reads mapping file
print STDERR "1. Parsing list of local files\n";
my $map_r = &parseMapFile("$folder/_src/map.txt", "$folder/_src/oldmap.txt");

####################################
# Parse the stat file: 
# for each line, if "Complete Genome", get the ftp folder
# if not force, compare the dates, if ftp newer, get it
# parse the LOCUS and DEFINITION lines, set correct folder (create if 
# necessary),
print STDERR "2. Parsing list of assemblies\n";
my $asms_r = &parseStatFile($ftpserver, $statfile, 
			    $folder, "assembly_summary_genbank.txt");
my $n = 0;

####################################
# Obtaining files
print STDERR "3. Downloading files by rsync\n";
my $changedfolders_r = &getFiles($asms_r, $rsyncserver, $genomesfolder, 
				 "$folder/_src/") unless $localonly;
my $nchanged = keys %{ $changedfolders_r };
print STDERR "   Retrieved files in $nchanged folders\n";

####################################
# Organizing files
print STDERR "4. Organizing files, splitting\n";
# Prepares writing new map
open NEWMAP, '>', "$folder/_src/map.txt" or 
  die "Cannot write to $folder/_src/map.txt";
# Print title for output file
print "#asm\t", join("\t", @outfields), "\tbn\tsrcfolder\torgfolder\n";
foreach my $asm (sort keys %{$asms_r}){
  # Skipping?
  next if ($skip && $n <= $skip);
  ### Some essential variables
  my $local_path = $asms_r->{$asm}{'ftp_path'};
  $local_path =~ s|^ftp://$ftpserver/$genomesfolder/||;
  my $bn = basename($local_path);
  $n++;
  if ($debug) {
    print STDERR "   Getting orgfolder\n" if $debug;
  } else {
    print STDERR "   Scanned $n/$ncomplete assemblies";
  }
  ### Get human-readable folder names
  my $orgfolder = &makeUserFriendly($local_path, $bn);
  $asms_r->{$asm}{'orgfolder'} = $orgfolder;
  # Checks that orgfolder is still the same
  if ($map_r->{$bn} && $map_r->{$bn} ne $orgfolder){
    warn "Orgfolder for $asm has changed:\n  was $map_r->{$asm}\n" . 
      "  now $orgfolder\nClean manually\n";
  }
  print NEWMAP "$local_path\t$orgfolder\n";
  ### Checks that the files haven't changed since last update
  $changedfolders_r->{$asm}++ 
    unless (-e "$folder/$orgfolder" && 
	    &isFolderUnchanged("$folder/$orgfolder",
			       "$folder/_src/$local_path"));
  ### Prints tab report
  print "$asm\t";
  foreach (@outfields){
    my $f = $asms_r->{$asm}{$_};
    print $f if $f;
    print "\t";
  }
  print "$bn\t$folder/_src/$local_path\t$folder/$orgfolder\n";
  ### Makes orgfolder, split files
  if ($force || $changedfolders_r->{$asm}){
    if ($debug){
      print STDERR "   Create folders and split files\n";
    } else {
      print STDERR ", processed $nsplitfolders folders";
    }
    $nsplitfolders++;
    &splitFiles($folder, $bn, $orgfolder, $local_path);
  }
  print STDERR "\r" unless ($debug);
}
# Close connection
#$ftp->quit;
$nchanged = keys %{ $changedfolders_r };
print STDERR "\n   Done. Scanned $n files, updated files in $nchanged folders,",
  " processed $nsplitfolders folders\n";
exit 1;

################################################################################
# Subs
################################################################################
## Parse stat file, returns a hash ref with the info required
sub parseStatFile {
  my ($ftp_server, $remote_statfile, $local_path, $statfile) = @_;
  my %asms;
  # Get the assembly_summary_genbank.txt file
  if (($debug || $uselocalstat) && -e "$local_path/$statfile"){
    print STDERR "  Not downloading again stats file $remote_statfile " . 
      "in debug or use-only-local-stats mode\n";
  } else {
    my $ff = File::Fetch->new(uri => "ftp://$ftpserver$remote_statfile");
    my $where = $ff->fetch( to => $local_path ) or die $ff->error;
  }
  # $ftp->get($remote_statfile, $local_statfile) 
  #   or die "get failed ", $ftp->message unless ($debug || $uselocalstat);
  print STDERR "  Reading  stats file $local_path/$statfile\n" if $debug;
  open TAB, '<', "$local_path/$statfile"
    or die "Could not open $local_path/$statfile: $!";
  while (<TAB>){
    next if /^#/;
    chomp;
    my ($assembly_accession, $bioproject, $biosample, $wgs_master, 
	$refseq_category, $taxid, $species_taxid, $organism_name, 
	$infraspecific_name, $isolate, $version_status, $assembly_level, 
	$release_type, $genome_rep, $seq_rel_date, $asm_name, $submitter, 
	$gbrs_paired_asm, $paired_asm_comp, $ftp_path) = split(/\t/, $_);
    die "Not all fields filled in line $_\n" 
      unless ($ftp_path && $assembly_level && $version_status);
    $nrows++;
    # Keep only "Complete Genome"
    next unless ($assembly_level eq "Complete Genome" 
		 && $version_status eq "latest");
    unless ($ftp_path =~ m|^ftp://|) {
      warn "  No ftp path in $assembly_accession. Skipping\n";
      next;
    }
    $ncomplete++;
    # Debug
    last if ($debug && $ncomplete >= $debug);
    # Store
    $asms{$assembly_accession}{'bioproject'} = $bioproject;
    $asms{$assembly_accession}{'biosample'} = $biosample;
    $asms{$assembly_accession}{'wgs_master'} = $wgs_master;
    $asms{$assembly_accession}{'refseq_category'} = $refseq_category;
    $asms{$assembly_accession}{'taxid'} = $taxid;
    $asms{$assembly_accession}{'species_taxid'} = $species_taxid;
    $asms{$assembly_accession}{'organism_name'} = $organism_name;
    $asms{$assembly_accession}{'infraspecific_name'} = $infraspecific_name;
    $asms{$assembly_accession}{'isolate'} = $isolate;
    $asms{$assembly_accession}{'version_status'} = $version_status;
    $asms{$assembly_accession}{'assembly_level'} = $assembly_level;
    $asms{$assembly_accession}{'release_type'} = $release_type;
    $asms{$assembly_accession}{'genome_rep'} = $genome_rep;
    $asms{$assembly_accession}{'seq_rel_date'} = $seq_rel_date;
    $asms{$assembly_accession}{'asm_name'} = $asm_name;
    $asms{$assembly_accession}{'submitter'} = $submitter;
    $asms{$assembly_accession}{'gbrs_paired_asm'} = $gbrs_paired_asm;
    $asms{$assembly_accession}{'paired_asm_comp'} = $paired_asm_comp;
    $asms{$assembly_accession}{'ftp_path'} = $ftp_path;
  }
  # Update user
  print STDERR "  Checked $nrows assemblies from NCBI, ",
    "$ncomplete were complete genomes\n";
  return \%asms;
}
## Newer getFiles with rsync
sub getFiles {
  my ($asms_r, $rsyncserver, $genomesfolder, $localfolder) = @_;
  # Include array: all folders with a +, followed by the file types
  # with asterisks in front and +, and finally "- *" to prevent getting the rest
  my @include;
  my %bns;
  my %changed;
  my %subfolders;
  my $nchanged = 0;
  my $n = 0;
  foreach my $asm (sort keys %{$asms_r}){
    $n++;
    next if ($skip && $n <= $skip);
    #last if ($n > 5); ## DEBUG
    my $ftp_path = $asms_r->{$asm}{'ftp_path'};
    $ftp_path =~ s|^ftp://$ftpserver/$genomesfolder/||;
    my @subfolders = split('/', $ftp_path);
    foreach my $i (0..3){
      my $subfolder = join('/', @subfolders[0..$i]);
      push @include, "+ $subfolder" unless $subfolders{$subfolder};
      $subfolders{$subfolder}++;
    }
    #my $bn = basename($ftp_path);
    push @include, "+ $ftp_path";
    $bns{"$ftp_path"}++;
  }
  foreach my $type (@filetypes){
    push @include, "+ *$type.gz";
  }
  push @include, '- *';
  # Create Rsync object, and run
  my $rsync = File::Rsync->new( archive  => 1, 
				compress => 0, 
				#quiet    => 1, 
				verbose  => 1, 
				partial  => 1,
				delete   => 1,
			      );
  $rsync->exec( src     => "rsync://$rsyncserver/$genomesfolder/", 
		dest    => $localfolder,
		include => \@include) 
    or die "Rsync failed";
  # Get the list of folders that changed, parse to get a hash to be returned
  my $out_r = $rsync->out();
  foreach my $line (@{ $out_r }){
    chomp $line;
    $line =~ s/\/+$//;
    if ($bns{$line}){
      $changed{$line}++;
      $nchanged++;
    }
  }
  return \%changed;
}

## Test if there is any file in a folder newer than the oldest file in another
# folder
sub isFolderUnchanged {
  my ($orgfolder, $srcfolder) = @_;
  my $hasChanged = 0;
  my $nfiles = 0;
  my $oldestInOrg = 9999999999;
  my $newestInSrc = 0;
  opendir DIR, "$orgfolder" 
    or die "Could not open $orgfolder: $!\n";
  my @files = readdir DIR;
  close DIR;
  foreach my $file (@files){
    next if $file =~ /^\.+/;
    $nfiles++;
    my $sb = stat("$orgfolder/$file");
    $oldestInOrg = $sb->mtime if ($sb->mtime < $oldestInOrg);
  }
  opendir DIR, "$srcfolder" 
    or die "Could not open $srcfolder: $!\n";
  @files = readdir DIR;
  close DIR;
  foreach my $file (@files){
    next if $file =~ /^\.+/;
    my $sb = stat("$srcfolder/$file");
    $newestInSrc = $sb->mtime if ($sb->mtime > $newestInSrc);
  }
  $hasChanged++ if ($oldestInOrg < $newestInSrc);
  # If there are less than 4 files, consider it has changed
  $hasChanged++ unless $nfiles > 3;
  ## DEBUG
  # if ($hasChanged > 0){
  #   use POSIX qw(strftime);
  #   print STDERR "In Org $orgfolder:\n  ", 
  #     strftime("%Y-%m-%d %H:%M:%S", localtime($oldestInOrg)), "\n";
  #   print STDERR "In Src $srcfolder:\n  ",  
  #     strftime("%Y-%m-%d %H:%M:%S", localtime($newestInSrc)), "\n";
  #   print STDERR "\n";
  # }
  return !$hasChanged;
}


## For a assembly folder, extracts the LOCUS and DEFINITION info
sub makeUserFriendly {
  my ($local_path, $bn) = @_;
  # Get DEFINITION lines
  my $gbkfile = "$folder/_src/$local_path/$bn" . "_genomic.gbff.gz";
  unless (-e $gbkfile){
    warn "Cannot find gbk file $gbkfile\n";
    return 0;
  }
  my @defs = &parseDefinitions("$folder/_src/$local_path/$bn" .
			       "_genomic.gbff.gz");
  my $def = &findCommonDefinition($bn, @defs);
  my $orgfolder = &orgFolderName($def);
  #print "$bn\t$def\n";
  return $orgfolder;
}

## Parse DEFINITION lines in a gbk file
sub parseDefinitions {
  my ($file) = @_;
  open my $fh, '<:gzip', $file or die "Cannot open $file: $!\n";
  my @defs;
  my $in_def;
  my $def;
  while (<$fh>){
    chomp;
    if (/^DEFINITION/){
      $def = $_;
      $def =~ s/DEFINITION\s*//;
      $in_def++;
    } elsif ($in_def && /^\s{10}/) {
      s/^\s+/ /;
      $def .= $_;
    } elsif ($in_def) {
      push @defs, $def unless $def =~ / phage /;
      $in_def = 0;
    }
  }
  return @defs;
}

## Find common string in DEFINITION lines
sub findCommonDefinition {
  my ($bn, @defs) = @_;
  my @suffixes = ("whole genome shotgun sequence",
		  "complete genome", "draft genome", "complete sequence", 
		  "complete", "genome", "plasmid", "chromosome", "DNA", );
  # First remove suffixes
  foreach my $i (0..$#defs){
    foreach my $suffix (@suffixes){
      $defs[$i] =~ s/^$suffix\s*//;
      $defs[$i] =~ s/[\.,;:\s]+$suffix.*$//;
    }
  }
  my $definition;
  if ($#defs == 0){
    $definition = $defs[0];
  } else {
    my @a;
    my $shortest = 10000;
    foreach my $i (0..$#defs){
      my @letters = split(//, $defs[$i]);
      $shortest = $#letters if ($#letters < $shortest);
      push @a, [ @letters ];
    }
    my $stopat = $shortest;
    for my $j (0..$shortest){
      foreach my $i (1..$#defs){
	if ($a[$i][$j] ne $a[0][$j]){
	  $stopat = $j;
	  last;
	}
      }
      last if $stopat < $shortest;
    }
    if ($stopat < 15) {
      print "No or short common words in $bn: ", join(" | ", @defs), 
	", using the first def in genbank file: $defs[0]\n" if $debug;
      $definition = $defs[0];
    } else {
      $definition = substr($defs[0], 0, $stopat);
    }
  }
  print "Short definition for $bn: $definition\n" 
    if ($debug && length($definition) < 20);
  return $definition;
}
## Return folder name
sub orgFolderName {
  my ($name) = @_;
  $name =~ s/[^a-zA-Z0-9_]+/_/g;
  $name =~ s/_+$//;
  return $name;
}

## Parse mapping file, save to another name
sub parseMapFile {
  my ($newmap, $oldmap) = @_;
  my %map;
  if (-e $newmap){
    move($newmap, $oldmap) or die ("Could not move map $newmap to $oldmap\n");
    open MAP, '<', $oldmap;
    while (<MAP>){
      chomp;
      my ($asm, $orgfolder) = split;
      die "Not all fields in $_\n" unless ($asm && $orgfolder);
      $map{$asm} = $orgfolder;
    }
  }
  return \%map;
}

## Splitting genbank, gff and fasta files
sub splitFiles {
  my ($folder, $bn, $orgfolder, $local_path) = @_;
  my $srcfolder = "$folder/_src/$local_path";
  my $trgtfolder = "$folder/$orgfolder";
  mkdir($trgtfolder) or die "Cannot mkdir $trgtfolder\n"
    unless (-e "$trgtfolder");
  my $prot_r = splitFile("$srcfolder/$bn\_genomic.gbff.gz", $trgtfolder, "gbk");
  splitFile("$srcfolder/$bn\_genomic.gff.gz", $trgtfolder, 'gff');
  splitFile("$srcfolder/$bn\_genomic.fna.gz", $trgtfolder, "fna");
  splitFaaFile("$srcfolder/$bn\_protein.faa.gz", $trgtfolder, $prot_r);
  return 1;
}

## Split
sub splitFile {
  my ($file, $trgtfolder, $type) = @_;
  my %fss = ( 'gff' => '##sequence-region',
	      'fna' => '>',
	      'gbk' => 'LOCUS      ');
  my $fs = $fss{$type};
  my %map;
  local $/ = $fs;
  unless (-e $file){
    warn "Cannot find $file\n" if $debug;
    return 0;
  }
  open FILE, '<:gzip', $file or die "Cannot open file $file $!\n";
  my $first = <FILE>;
  #chomp($first);
  while (<FILE>){
    chomp;
    # remove potential fs in the beginning
    s/^$fs//;
    # Find the id
    /^(\s*)(\w+)(\.\d+)?\s+/;
    my $spaces = $1;
    my $id = $2;
    die "Could not parse id in $file\n" unless $id;
    open OUT, '>', "$trgtfolder/$id.$type" 
      or die "Cannot open $trgtfolder/$id.$type: $!\n";
    print OUT $first . $_;
    close OUT;
    # Fetches prot ids in gbk files
    if ($type eq 'gbk'){
      my @protids = ( $_ =~ /protein_id="(\w+)\.?\d*"/g );
      foreach my $protid (@protids) {
	$map{$protid} = $id;
      }
    }
  }
  return \%map;
}

## Split faa file. Needs map for that
sub splitFaaFile {
  my ($file, $trgtfolder, $map_r) = @_;
  local $/ = '>';
  unless (-e $file){
    warn "Cannot find $file\n" if $debug;
    return 0;
  }
  open FILE, '<:gzip', $file or die "Cannot open file $file $!\n";
  my $first = <FILE>;
  while (<FILE>){
    chomp;
    # remove potential fs in the beginning
    s/^>//;
    # Find the id
    /^(\w+)(\.\d+)?\s+/;
    my $protid = $1;
    die "Could not parse id in $file\n" unless $protid;
    die "Protid $protid not in map\n" unless $map_r->{$protid};
    my $id = $map_r->{$protid};
    open OUT, '>>', "$trgtfolder/$id.faa" 
      or die "Cannot open $trgtfolder/$id.faa: $!\n";
    print OUT '>' . $_;
    close OUT;
  }
}
